import tensorflow as tf
from tensorflow import keras
from keras import layers
from tensorflow.keras import callbacks
import kerastuner
from kerastuner.tuners import RandomSearch
import tensorflow_addons as tfa
from kerastuner.engine.hyperparameters import HyperParameters
import datetime
from custom_metrics import f1, precision

#building dynamic model
def build_model(hp):

    model = tf.keras.Sequential()
    
    #scaling imgs [0-255] -> [0-1]
    model.add(layers.experimental.preprocessing.Rescaling(1./255))

    #convolutional layers and Pooling with activation function relu
    for x in range(hp.Int('num_conv_layers', 1, 4)):
        model.add(layers.Conv2D(filters=hp.Int('filters_act_' + str(x),
                                            min_value=16,
                                            max_value=260,
                                            step=16),
                                            kernel_size = 3,
                                            activation='relu'))
        model.add(layers.MaxPooling2D())

    model.add(layers.Flatten())

    #dense layers with activation function relu
    for y in range(hp.Int('num_dense_layers', 0, 4)):
        model.add(layers.Dense(units=hp.Int('units_dense_' + str(y),
                                            min_value=16,
                                            max_value=260,
                                            step=32),
                                            activation='relu'))

    #fully connected layer
    
    model.add(layers.Dense(1, activation='sigmoid'))

    model.compile(
    optimizer='adam',
    loss='binary_crossentropy',
    metrics=['accuracy',
            precision,
           f1])
    return model


batch_size = 16
img_height = 180
img_width = 180
data_dir = '../dataset/crops_dataset_75/'

#get traning data
train_ds = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  validation_split=0.2,
  subset="training",
  seed=123,
  image_size=(img_height, img_width),
  batch_size=batch_size)

#get validation data
val_ds = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  validation_split=0.2,
  subset="validation",
  seed=123,
  image_size=(img_height, img_width),
  batch_size=batch_size)


#initializing Hyperparametertuner
tuner = RandomSearch(
    build_model,
    objective= "val_accuracy",
    max_trials=40,
    executions_per_trial=1,
    directory='tf_dir',
    project_name = 'model_logs'
)

#building models
tuner.search(
    train_ds,
    epochs=4,
    validation_data = val_ds
)

tuner.results_summary()