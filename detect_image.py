import cv2
from face_detector.face_detector import FaceDetector
import numpy as np

class ImageDetection:
    def __init__(self):
        super().__init__()
        self.labelsDict = {0: 'no mask', 1: 'mask'}

    def detection(self, filepath, model, proba):
        image = cv2.imread(filepath)
        #print(proba)
        proba = proba/100
        face_detector = FaceDetector(proba)
        detections = face_detector.detectFaces(image)
        (h, w) = image.shape[:2]
        # check if all under confidence
        confidence_values = detections[0, 0 , : , 2]
        def check(l):
            for e in l:
                if e > proba:
                    return True
            return False
        #print(check(confidence_values))
        if check(confidence_values):
            # loop over the detections
            for i in range(0, detections.shape[2]):
                # extract the confidence (i.e., probability) associated with the
                # prediction
                confidence = detections[0, 0, i, 2]
                # filter out weak detections by ensuring the `confidence` is greater than the minimum confidence
                if confidence > face_detector.confidenceMin:
                    # compute the (x, y) coordinates of the bounding box for the object
                    box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                    (startX, startY, endX, endY) = box.astype("int")
                    #reset to image border if the detected area is outside the image
                    if startX < 0:
                        startX = 0
                    if startY < 0:
                        startY = 0
                    if endX > len(image[0]):
                        endX = len(image[0])
                    if endY > len(image):
                        endY = len(image)
                    # get cropped face for prediction model
                    face = image[startY:endY,startX:endX]
                    if (face.shape[0]==0 and face.shape[1]==0):
                        pass
                    else:
                        preprocessed_img = cv2.resize(face,(180, 180),interpolation=cv2.INTER_LINEAR )
                        preprocessed_img = cv2.cvtColor(preprocessed_img, cv2.COLOR_BGR2RGB )
                        mask_prediction = model.predict(np.array([preprocessed_img]))
                        mask_val = 0
                        if mask_prediction[0][0] > 0.5:
                            mask_val = 1
                        else:
                            mask_val = 0
                        label = self.labelsDict[mask_val]
                        # draw the bounding box of the face along with the associated probability
                        text = "{:.2f}%".format(confidence * 100)
                        y = startY - 10 if startY - 10 > 10 else startY + 10
                        if label=="no mask":
                            cv2.rectangle(image, (startX, startY), (endX, endY), (0, 0, 255), 2)
                            cv2.putText(image, label, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)
                        else:
                            cv2.rectangle(image, (startX, startY), (endX, endY), (0, 255, 0), 2)
                            cv2.putText(image, label, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 2)
        else:
            # cv2.rectangle(image, (round(w/2 - w/3),round(h/2 - h/10)), (round(w/2 + w/3),round(h/2 + h/10)), (0, 0, 255), 2)
            cv2.putText(image, "no face detected", (round(w/2 - w/3),round(h/2)), cv2.FONT_HERSHEY_SIMPLEX, 5, (0, 0, 255), 3)
        return image