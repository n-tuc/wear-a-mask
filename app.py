import tkinter as tk
from tkinter.filedialog import askopenfilename, asksaveasfilename
from PIL import Image, ImageTk
import cv2
from face_detector.face_detector import FaceDetector
import pathlib
from pathlib import Path
import tensorflow as tf
from training.custom_metrics import precision, recall, f1
from imutils.video import VideoStream
import numpy as np
from detect_video import VideoDetection
from detect_image import ImageDetection

model_dir = str(Path.joinpath(Path.cwd(), 'model/final_model'))
model = tf.keras.models.load_model(model_dir,custom_objects={'precision':precision, 'recall': recall, "f1":f1})

def home():
    canvas.delete("imported_image")
    canvas.create_text(250, 250, anchor='nw', text= text,fill="black",font=('Helvetica 15 bold'), tags="welcome_text")

def open_image():
    global img
    global filepath
    global canvas_image
    global dim
    canvas.delete("imported_image")
    canvas.delete("welcome_text")

    filepath = askopenfilename(
        filetypes=[("Image Files", "*.jpg *.png"), ("All Files", "*.*")]
    )
    if not filepath:
        return
    # show image to window, scaling when too big
    cv_image = cv2.imread(filepath)
    if (cv_image.shape[1] >2500) or (cv_image.shape[0]>2500):
        scale_percent = 35 # percent of original size
        width = int(cv_image.shape[1] * scale_percent / 100)
        height = int(cv_image.shape[0] * scale_percent / 100)
        dim = (width, height)
    elif (cv_image.shape[1] >1000) or (cv_image.shape[0]>1000):
        scale_percent = 70 # percent of original size
        width = int(cv_image.shape[1] * scale_percent / 100)
        height = int(cv_image.shape[0] * scale_percent / 100)
        dim = (width, height)
    else:
        dim = (cv_image.shape[1], cv_image.shape[0])
    cv_img_to_display = cv2.cvtColor(cv2.resize(cv_image, dim), cv2.COLOR_BGR2RGB)
    image = Image.fromarray( cv_img_to_display)
    img = ImageTk.PhotoImage(image)  
    canvas_image = canvas.create_image(0,0, anchor=tk.NW, image=img, tags="imported_image")

def predict_image():
    global filepath
    global canvas_image
    global updated_img
    global dim
    global w
    
    confidence = w.get()
    # get updated image with boundingbox
    img_detector = ImageDetection()
    
    updated_img = img_detector.detection(filepath, model, confidence)
    # transform to rgb to display on tkinter canvas
    # after label writting, size reduction for canvas
    cv_updated_img = cv2.cvtColor(cv2.resize(updated_img, dim), cv2.COLOR_BGR2RGB)
    cv_updated_img = Image.fromarray(cv_updated_img)
    updated_img = ImageTk.PhotoImage(cv_updated_img)
    canvas.itemconfig(canvas_image, image=updated_img)
    #except:
    #    text = "no face was detected"
    #    canvas.create_text(800, 800, anchor='nw', text= text,fill="black",font=('Helvetica 15 bold'))

def start_camera():
    # using extra window to open the application with video
    global stream
    global w
    
    confidence = w.get()
    stream = VideoDetection(model)
    stream.detection(confidence)
    return True
def stop_camera():
    global stream 
    stream.stop()
    return True
"""
Window Application
"""
window = tk.Tk()
window.title("Mundschutz-Erkennung")
window.rowconfigure(0, minsize=800, weight=1)
window.columnconfigure(1, minsize=800, weight=1)

"""define all widget types"""
fr_buttons = tk.Frame(window, relief=tk.RAISED, bd=2)
canvas = tk.Canvas(window, width = 800, height = 800)

"""define all components"""
btn_home = tk.Button(fr_buttons, text="Home", command=home)
btn_img = tk.Button(fr_buttons, text="Open new image", command=open_image)
btn_pred_img = tk.Button(fr_buttons, text="Predict image", command=predict_image)
btn_camera = tk.Button(fr_buttons, text="Start camera", command=start_camera)
btn_stop_camera = tk.Button(fr_buttons, text="Stop camera", command=stop_camera)
w = tk.Scale(fr_buttons, from_=0, to=100, orient="horizontal")
w.set(40)

"""set welcome window with canvas"""
text = "Projektseminar: Mund-Nasen-Schutz-Erkennung \n\n Sommersemester 2021 \n Göbel, Luu, Stötzner"
canvas.create_text(250, 250, anchor='nw', text= text,fill="black",font=('Helvetica 15 bold'), tags="welcome_text")

"""
define positions of buttons: row and column 0
layout has two columns, one for buttons and one for display the content, ex text or image
"""

btn_home.grid(row=0, column=0, sticky="ew", padx=5, pady=5)
btn_img.grid(row=1, column=0, sticky="ew", padx=5, pady=5)
btn_pred_img.grid(row=2, column=0, sticky="ew", padx=5, pady=5)
btn_camera.grid(row=3, column=0, sticky="ew", padx=5, pady=5)
btn_stop_camera.grid(row=4, column=0, sticky="ew", padx=5, pady=5)
w.grid(row=5, column=0, sticky="ew", padx=5, pady=5)

fr_buttons.grid(row=0, column=0, sticky="ns")
canvas.grid(row=0, column=1, sticky="nsew")

window.mainloop()