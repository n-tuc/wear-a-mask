# Wear A Mask

Ein Python Projekt im Rahmen des Projektseminars E-Business der Martin-Luther-Universität Halle. Das Programm kann auf einem Foto oder einem Stream durch Kamera erkennen, ob jemand einen Mund-Nasen-Schutz trägt.

## Tools und Technologien
1. Python Programmiersprache und Paketten
2. Datensammlung mit drei Suchmaschinen: Bing, Google, Pexels
3. Caffe Deep Learning Framework
4. OpenCV
5. Tensorflow, Tensorboard und Keras
6. Scikit-Learn
7. Klassifikationsmodell mit Convolutional Neural Network

## Installation und Starten der Anwendung

1. [Installation von Anaconda auf dem System](https://docs.anaconda.com/anaconda/install/index.html)
2. Klonen des Projektes aus Gitlab und wechseln mit der Konsole in den Projektsordner
3. Erstellen der `wamProject` Entwicklungsumgebung und ihren abhängigen Paketen aus der Datei requirements.yml mit dem Befehl: `conda env create -f requirements.yml`. Mit Windows kann man außerdem direkt die Anaconda Oberfläche anwenden. Es dauert einige Minuten, bis alle Abhängigkeiten fertig installiert wurden.

- [X] Wenn man ein Windows System hat, kann man die zur Verfügung gestellten Datei `requirements_example_window.yml` im Projektsordner anwenden `conda env create -f requirements_example_window.yml`.

- [X] Falls eine Warnung wegen `pip` angezeigt wird, wird Anaconda automatisch diese Abhängigkeit dazu installieren. Man kann diese Warnung ignorieren oder `pip` in der Datei `requirements.yml` unter `dependencies` eintragen.

- [X] Wenn ein Error mit `tensorflow.python.keras.utils` auftritt, sollte Tensorflow reinstalliert werden. Befehl: `pip install tensorflow --upgrade --force-reinstall --user`

- [X] Wenn die Erstellung der Umgebung mit dem Fehler `ResolvePackageNotFound` gescheitert ist, liegt es daran, dass einige Abhängigkeiten mit spezifischen Versionen für das Linux-System verlangt sind. Wenn man ein andere Betriebssysteme hat, kann man diese angezeigten Pakette `ResolvedPackageNotFound` in der Datei `requirements.yml` mit `#` auskommentieren. Man startet dann den Befehl: `conda env create -f requirements.yml` erneut.

4. Wenn die Erstellung der Entwicklungsumbegung erfolgreich ist, aktiviert man die Umgebung mit dem Befehl: `conda activate wamProject`
5. Starten der Applikation mit `python app.py`.

## Benutzeroberfläche
![Userinterface](examples/interface_2.png)
1. **Importieren** eines Bildes durch **Open new image**
2. Die Klassifikation erfolgt über das Klicken von **Predict**.
3. Mit dem Schieberegler kann man den Konfidenzwert der Gesichtserkennung ändern.
  - Konfidenzwert mittels Schieberegler ändern
  - **Predict** erneut klicken

## Starten des Tensorboards

1. Starten der virtuellen Umgebung (siehe Abschnitt **Installation**)
2. In dem Projektsordner: `tensorboard --logdir training/logs/` in der Konsole eingeben
3. Welchseln in den Browser zu http://localhost:6006/

## Tipp für Exportieren der requirements.yml
Wenn eine Abhängigkeit mit einer spezifischen Version, die man lokal installierte, exportiert wird, kann es passieren, dass diese Abhängikeit nicht bei einer anderen Plattform oder in einem anderen Betriebssystem gefunden wird. Aus diesem Grund wird es empfohlen, die Abhängigkeiten der virtuellen Umgebung ohne Version-Nummer zu exportieren.

1. Exportieren komplex ohne Version-Nummer und Prefix: `conda env export | cut -f 1 -d '=' | grep -v "prefix" > requirements.yml`
2. Exportieren ohne Plattform-Version-Nummer: `conda env export --no-builds | grep -v "prefix" > requirements.yml`
