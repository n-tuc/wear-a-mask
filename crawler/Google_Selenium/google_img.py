#Webdriver https://stackoverflow.com/questions/22476112/using-chromedriver-with-selenium-python-ubuntu
#Selenium https://selenium-python.readthedocs.io/installation.html
#check Google Chrome Version Ubuntu: google-chrome --version
#Download correct version and move to user/bin/: https://stackoverflow.com/questions/48649230/how-to-update-chromedriver-on-ubuntu
#Downloads correct version https://sites.google.com/a/chromium.org/chromedriver/downloads, https://chromedriver.storage.googleapis.com/index.html?path=89.0.4389.23/

from selenium import webdriver
import time
import requests
import shutil
import os
from tqdm import tqdm

user = os.getlogin()
driver = webdriver.Chrome("/usr/bin/chromedriver")
directory = os.getcwd()

#inputs = ['person', 'people', 'corona masks', 'corona masked faces']
inputs = ['person face', 'person', 'corona masks', 'covid masked face']

for i in tqdm(range(0,len(inputs))):

    inp = inputs [i]
    #url = 'https://www.google.com/search?q='+str(inp)+'&sxsrf=ALeKk00elKl1LJJrC4x47YKFiAcYOibBYg:1620401900230&source=lnms&tbm=isch&sa=X&ved=2ahUKEwi92p-987fwAhVLyKQKHZFRCIEQ_AUoA3oECAEQBQ&biw=960&bih=442'
    url = 'https://www.google.com/search?q='+str(inp)+'&source=lnms&tbm=isch&sa=X&ved=2ahUKEwie44_AnqLpAhUhBWMBHUFGD90Q_AUoAXoECBUQAw&biw=1920&bih=947'
    iterate = 500
    
    if "corona" in inp or "covid" in inp:
        folder = directory+"/data/masked_faces/"
    else:
        folder = directory+"/data/human_faces/"

    def save_img(inp,img,i):
        try:
            filename = inp+str(i)+'.jpg'
            response = requests.get(img,stream=True)
            image_path = os.path.join(folder, filename)
            with open(image_path, 'wb') as file:
                shutil.copyfileobj(response.raw, file)
        except Exception:
            pass

    def find_urls(inp,url,driver,iterate):
        driver.get(url)
        time.sleep(2)

        #scroll down to the end:
        
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        time.sleep(5)
        try:
            driver.find_element_by_xpath('//*[@id="islmp"]/div/div/div/div/div[4]/div[2]/input').click()
        except Exception:
            pass

        for j in tqdm(range (5,iterate, 5)):
            try:
                imgurl = driver.find_element_by_xpath('//div//div//div//div//div//div//div//div//div//div['+str(j)+']//a[1]//div[1]//img[1]')
                #//*[@id="Sva75c"]/div/div/div[3]/div[2]/c-wiz/div/div[1]/div[1]/div/div[2]/a/img
                #//*[@id="Sva75c"]/div/div/div[3]/div[2]/c-wiz/div/div[1]/div[1]/div/div[2]/a/img
                imgurl.click()
                time.sleep(5)
                try:
                    img = driver.find_element_by_xpath('//*[@id="Sva75c"]/div/div/div[3]/div[2]/c-wiz/div/div[1]/div[1]/div/div[2]/a/img').get_attribute("src")
                    save_img(inp,img,j)
                except Exception:
                    pass
            except Exception:
                pass

    find_urls(inp,url,driver,iterate)
