# import libraries
import cv2
import time
import imutils
import tensorflow as tf
import numpy as np
from imutils.video import VideoStream
from training.custom_metrics import precision, recall, f1
import pathlib
from pathlib import Path

class VideoDetection:
   
    def __init__(self, model):
        #net = cv2.dnn.readNetFromCaffe('deploy.prototxt.txt','res10_300x300_ssd_iter_140000.caffemodel') 
        # load serialized model from disk and initialize the video stream
        caffemodelPath = str(Path.joinpath(Path.cwd(), 'face_detector/res10_300x300_ssd_iter_140000.caffemodel'))
        prototxtPath = str(Path.joinpath(Path.cwd(), 'face_detector/deploy.prototxt.txt'))
        model_dir = str(Path.joinpath(Path.cwd(), 'model/final_model'))

        self.net = cv2.dnn.readNetFromCaffe(prototxtPath, caffemodelPath)
        time.sleep(2.0)
        self.model = model ##tf.keras.models.load_model(model_dir,custom_objects={'precision':precision, 'recall': recall, "f1":f1})        
        self.labelsDict = {0: 'no mask', 1: 'mask'}
        self.window = True
    # loop over the frames from the video stream
    def detection(self, proba):
        proba = 0.5
        self.vs = VideoStream(src=0).start()
        while self.window:
            # grab the frame from the threaded video stream and resize it to have a maximum width of 400 pixels
            frame = self.vs.read()
            frame = imutils.resize(frame)
        
            # grab the frame dimensions and convert it to a blob
            (h, w) = frame.shape[:2]
            blob = cv2.dnn.blobFromImage(cv2.resize(frame, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0))
        
            # pass the blob through the network and obtain the detections and predictions
            self.net.setInput(blob)
            detections = self.net.forward()

            
            # loop over the detections
            for i in range(0, detections.shape[2]):
                # extract the confidence (i.e., probability) associated with the prediction
                confidence = detections[0, 0, i, 2]

                # filter out weak detections by ensuring the `confidence` is greater than the minimum confidence
                # you can also change the 'confidence' (0.5 here) for better results
                if confidence < proba:
                    continue

                # compute the (x,y) coordinates of the bounding box for the object
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                (startX, startY, endX, endY) = box.astype("int")
                
                ##############################################
                #detect mask on crop

                if startX < 0:
                    startX = 0
                if startY < 0:
                    startY = 0
                if endX > len(frame[0]):
                    endX = len(frame[0])
                if endY > len(frame):
                    endY = len(frame)
                croppedFace = frame[startY:endY,startX:endX]
                preprocessed_img = cv2.resize(croppedFace, (180, 180),interpolation=cv2.INTER_LINEAR )
                preprocessed_img = cv2.cvtColor(preprocessed_img, cv2.COLOR_BGR2RGB )
                
                frame_as_array = np.array([preprocessed_img])
                #decision
                #0 =no mask 1 = mask
                mask_prediction = self.model.predict(frame_as_array)
                mask_val = 0
                if mask_prediction[0][0] > 0.5:
                    mask_val = 1
                else:
                    mask_val = 0
                label = self.labelsDict[mask_val]
                
                # draw the bounding box of the face along with the associated probability
                text = label
                y = startY - 10 if startY - 10 > 10 else startY + 10
                if mask_val == 0:
                    cv2.rectangle(frame, (startX, startY), (endX, endY), (0, 0, 255), 2)
                    cv2.putText(frame, text, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
                else:
                    cv2.rectangle(frame, (startX, startY), (endX, endY), (0, 255, 0), 2)
                    cv2.putText(frame, text, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 255, 0), 2)

            # show the output frame
            cv2.imshow("Frame", frame)
            # Close windows with Esc
            key = cv2.waitKey(1) & 0xFF
        
            # break the loop if ESC key is pressed
            if key == 27:
               break
        
        # destroy all the windows
        # cv2.destroyAllWindows()
        # self.vs.stop()
    def stop(self):
        self.window=False
        cv2.destroyAllWindows()
        self.vs.stop()