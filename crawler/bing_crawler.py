from keys import key
import requests
import csv
import time


def load_search_terms (filename):
    search_terms = []
    with open(filename, newline='') as csvfile:
        queryreader = csv.reader(csvfile)
        print ('loading CSV')
        for row in queryreader:
            search_terms.append(row)
    return search_terms

def call_api (search_term):
    url = 'https://api.bing.microsoft.com/v7.0/images/search'
    headers ={"Ocp-Apim-Subscription-Key" : key}
    params  = {"q": search_term,  "imageType": "photo", "count" : "30"}
    response = requests.get(url, headers=headers, params=params)
    response.raise_for_status()
    search_results = response.json()
    print('got results from bing')
    return search_results['value']


search_terms_masked = []
search_terms_unmasked = []

#loading search terms from csv files
search_terms_masked = load_search_terms('data/search_terms_masked.csv')
search_terms_unmasked = load_search_terms('data/search_terms_unmasked.csv')

#requesting images from Bing Image Search API and saving them

i=0

#images of unmasked faces

for search_term in search_terms_unmasked:
    results = call_api(search_term)
    for result in results:
        try:
            contentUrl = result['contentUrl']
            image = requests.get(contentUrl, verify = False)
            file = open('../dataset/original/faces/reload' + str(i) + '.jpg', 'wb')
            print ('downloading image' + str(i))
            file.write(image.content)
            file.close()
            i +=1
        except OSError:
            pass

