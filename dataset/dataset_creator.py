import sys
sys.path.append("..") # Adds higher directory to python modules path.
from face_detector.face_detector import FaceDetector
import cv2
import imutils
import os
import skimage
from random import randint

NOISE_VAR = 0.03
#this python script creates a dataset with only faces on image
#dataset to learn the CNN 

faceDetector = FaceDetector(0.75)

def dataAugmentation(faceArray, folder, imgCount):
    faceCount = 0
    for face in faceArray:
        faceCount = faceCount + 1
        if len(face) != 0:
            try:
                rotationAngle = randint(1, 90) - 45
                rotated_face = imutils.rotate_bound(face, rotationAngle)
                noise_face = skimage.util.random_noise(face, mode='gaussian', var=NOISE_VAR)
                #noise_face = face + noise
                if folder == 'face':
                    cv2.imwrite('./crops_dataset/face/face' + str(imgCount) + '_' + str(faceCount) + '.jpg',face)
                    cv2.imwrite('./crops_dataset/face/face' + str(imgCount) + '_' + str(faceCount) + 'rotated.jpg',rotated_face)
                    cv2.imwrite('./crops_dataset/face/face' + str(imgCount) + '_' + str(faceCount) + 'noise.jpg',noise_face * 255.0)
                else:
                    cv2.imwrite('./crops_dataset/mask/mask' + str(imgCount) + '_' + str(faceCount) + '.jpg',face)
                    cv2.imwrite('./crops_dataset/mask/mask' + str(imgCount) + '_' + str(faceCount) + 'rotated.jpg',rotated_face)
                    cv2.imwrite('./crops_dataset/mask/mask' + str(imgCount) + '_' + str(faceCount) + 'noise.jpg',noise_face * 255.0)
            except:
                pass
       
#create crops 
path =r'./original/faces'
imgCount = 0
for root, dirs, img_paths in os.walk(path):
    for img_path in img_paths:
        try:
            croppedFaces = faceDetector.getCroppedFaces(path + '/' + img_path)
        except:
            #problem with image
            print(img_path)
        
        if len(croppedFaces) != 0:
            imgCount = imgCount + 1
            dataAugmentation(croppedFaces,'face',imgCount)
           
#create crops of image with masked faces
path =r'./original/masked_faces'
imgCount = 0
for root, dirs, img_paths in os.walk(path):
    for img_path in img_paths:
        try:
            croppedFaces = faceDetector.getCroppedFaces(path + '/' + img_path)
        except:
            #problem with image
            print(img_path)
        if len(croppedFaces) != 0:
            imgCount = imgCount + 1
            dataAugmentation(croppedFaces,'mask',imgCount)

print("Dataset created")
