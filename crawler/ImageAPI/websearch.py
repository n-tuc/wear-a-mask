import requests
from tqdm import tqdm
import time

class ContextualWebsearch:
    def __init__(self, api_name, id, query, keyfile, foldername):
        self.api_name = api_name
        self.query = query;
        self.id = id
        
        self.url = "https://contextualwebsearch-websearch-v1.p.rapidapi.com/api/Search/ImageSearchAPI"
        self.params = {"q":self.query,"pageNumber":"5","pageSize":"20","autoCorrect":"true"}
        self.key = open(keyfile, "r").read().rstrip()
        print(self.key)
        self.headers = {
            'x-rapidapi-key': self.key,
            'x-rapidapi-host': "contextualwebsearch-websearch-v1.p.rapidapi.com"
            }
        self.foldername = foldername
    def callApi(self):
        def search():
            try:
                response = requests.request("GET", self.url, headers=self.headers, params=self.params)
                response.raise_for_status()
                return response.json()
            except requests.exceptions.HTTPError:
                time.sleep(1)
                return search()
        resp = search()
        return resp['value']

    def get_images(self, imgs):
        print(self.query)
        for i in tqdm(range(0, len(imgs)), desc="Saving Images Process:"):
            img_url = imgs[i]['url']
            img_n = imgs[i]['title']
            img = requests.get(img_url)
            open('../data/'+self.foldername+'/'+self.api_name+"_"+str(self.id)+"_"+str(i)+ '.jpg', 'wb').write(img.content)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--QueryFile", type=str,
                        help="give filename of queries")

    parser.add_argument("-a", "--API", type=str,
                    help="give api which should be used")

    args = parser.parse_args()

    queries = open(args.QueryFile, "r").read().split("\n")
    print(queries)
    
    if args.API == "websearch":
        for i in tqdm(range(0, len(queries)), desc="Get responses for a query:"):
            api = ContextualWebsearch(args.API, i+1, queries[i],"key.txt", args.QueryFile.split(".")[0])
            resp = api.callApi()
            api.get_images(resp)