import cv2
import numpy as np
import pathlib
from pathlib import Path

class FaceDetector:
    caffemodelPath = str(Path.joinpath(Path.cwd(), 'face_detector/res10_300x300_ssd_iter_140000.caffemodel'))
    prototxtPath = str(Path.joinpath(Path.cwd(), 'face_detector/deploy.prototxt.txt'))

    #caffemodelPath = '../face_detector/res10_300x300_ssd_iter_140000.caffemodel'
    #prototxtPath = '../face_detector/deploy.prototxt.txt' 

    #caffemodelPath = './res10_300x300_ssd_iter_140000.caffemodel'
    #prototxtPath = 'deploy.prototxt.txt' 

    #confidence defines how confident the face detection is
    #e. g. confidence = 0.5, it's a face with a 50% confident
    def __init__(self,confidence):
        self.confidenceMin = confidence
        #loads the pretrained net
        self.net = cv2.dnn.readNetFromCaffe(FaceDetector.prototxtPath,FaceDetector.caffemodelPath)

    #its th actual detection
    def detectFaces(self, image):
        #prepares the images for classification, same size and mean color substraction
        blob = cv2.dnn.blobFromImage(cv2.resize(image, (300, 300)), 1.0,
    	(300, 300), (104.0, 177.0, 123.0))
        self.net.setInput(blob)
        detections = self.net.forward()
        return detections

    def getImageWithBoundingBoxes(self, imagePath):
        image = cv2.imread(imagePath)
        detections = self.detectFaces(image)
        (h, w) = image.shape[:2]
        # loop over the detections
        for i in range(0, detections.shape[2]):
            # extract the confidence (i.e., probability) associated with the
            # prediction
            confidence = detections[0, 0, i, 2]
           
            # filter out weak detections by ensuring the `confidence` is greater than the minimum confidence
            if confidence > self.confidenceMin:
                # compute the (x, y) coordinates of the bounding box for the object
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                (startX, startY, endX, endY) = box.astype("int")
                # draw the bounding box of the face along with the associated probability
                text = "{:.2f}%".format(confidence * 100)
                y = startY - 10 if startY - 10 > 10 else startY + 10
                cv2.rectangle(image, (startX, startY), (endX, endY), (0, 0, 255), 2)
                cv2.putText(image, text, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (255, 0, 0), 1)
        return image
    
    def getImageWithBoundingBoxesLabel(self, imagePath, labels):
        image = cv2.imread(imagePath)
        detections = self.detectFaces(image)
        (h, w) = image.shape[:2]
        # loop over the detections
        for i in range(0, detections.shape[2]):
            # extract the confidence (i.e., probability) associated with the
            # prediction
            confidence = detections[0, 0, i, 2]
           
            # filter out weak detections by ensuring the `confidence` is greater than the minimum confidence
            if confidence > self.confidenceMin:
                # compute the (x, y) coordinates of the bounding box for the object
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                (startX, startY, endX, endY) = box.astype("int")
                # draw the bounding box of the face along with the associated probability
                text = "{:.2f}%".format(confidence * 100)
                label = labels[i]
                y = startY - 10 if startY - 10 > 10 else startY + 10
                if label=="no mask":
                    cv2.rectangle(image, (startX, startY), (endX, endY), (255, 0, 0), 2)
                    #cv2.putText(image, text, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)
                    cv2.putText(image, label, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)
                else:
                    cv2.rectangle(image, (startX, startY), (endX, endY), (0, 255, 0), 2)
                    #cv2.putText(image, text, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 0, 255), 2)
                    cv2.putText(image, label, (startX, y), cv2.FONT_HERSHEY_SIMPLEX, 0.6, (0, 255, 0), 2)
        return image
    
    def getCroppedFaces(self, imagePath):
        image = cv2.imread(imagePath)
        detections = self.detectFaces(image)
        (h, w) = image.shape[:2]
        croppedFaces = []
        # loop over the detections
        for i in range(0, detections.shape[2]):
            # extract the confidence (i.e., probability) associated with the
            # prediction
            confidence = detections[0, 0, i, 2]
            
            # filter out weak detections by ensuring the `confidence` is greater than the minimum confidence
            if confidence > self.confidenceMin:
                # compute the (x, y) coordinates of the bounding box for the object
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                (startX, startY, endX, endY) = box.astype("int")
                #reset to image border if the detected area is outside the image
                if startX < 0:
                    startX = 0
                if startY < 0:
                    startY = 0
                if endX > len(image[0]):
                    endX = len(image[0])
                if endY > len(image):
                    endY = len(image)
                croppedFace = image[startY:endY,startX:endX]
                croppedFaces.append(croppedFace)
        return croppedFaces
