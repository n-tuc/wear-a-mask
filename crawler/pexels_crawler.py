import requests

search_string = input("Suchanfrage:") #1. corona mask
start_idx = int(input("Startindex:"))
#search request
url = 'https://api.pexels.com/v1/search?per_page=20&query=' + search_string
headers = {'Authorization': '563492ad6f9170000100000158ee1c5f61d24ccebbe1d4b11ba8d81a '}



#set start idx to prevent overwriting the images
idx = start_idx
#200 pictures per Request. 200 are allowed per hour
max_idx = start_idx + 200
while idx < max_idx:
    #api request
    response = requests.get(url, headers=headers).json()
    #get list of photo links
    photos = response['photos']
    for photo in photos: #20 photos --> while conditions works with 200
        img_url = photo['src']['medium']
        image_r = requests.get(img_url)
        open('../masked_faces/pexels' + str(idx) + '.jpg', 'wb').write(image_r.content)
        idx = idx + 1
    print(idx)
    #next result page
    url = response['next_page']
print("fertig")
