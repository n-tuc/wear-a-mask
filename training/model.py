import tensorflow as tf
from tensorflow import keras
from keras import layers
from tensorflow.keras import callbacks
import datetime
from custom_metrics import f1, precision

#building model
def build_model(conv_layers, conv_size, dense_layers, dense_size):

    model = tf.keras.Sequential()
    
    #scaling imgs [0-255] -> [0-1]
    model.add(layers.experimental.preprocessing.Rescaling(1./255))

    #convolutional layers and Pooling with activation function relu
    for x in range(conv_layers):
        model.add(layers.Conv2D(conv_size[x],
                                kernel_size = 3,
                                activation='relu'))
        model.add(layers.MaxPooling2D())

    model.add(layers.Flatten())

    #dense layers with activation function relu
    for y in range(dense_layers):
        model.add(layers.Dense(dense_size[y],
                                activation='relu'))

    #fully connected layer
    model.add(layers.Dense(1, activation='sigmoid'))

    model.compile(
    optimizer='adam',
    loss='binary_crossentropy',
    metrics=['accuracy',
            precision,
           f1])
    return model


batch_size = 16
img_height = 180
img_width = 180
data_dir = '../dataset/crops_dataset_75/'

#get traning data
train_ds = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  validation_split=0.2,
  subset="training",
  seed=123,
  image_size=(img_height, img_width),
  batch_size=batch_size)

#get validation data
val_ds = tf.keras.preprocessing.image_dataset_from_directory(
  data_dir,
  validation_split=0.2,
  subset="validation",
  seed=123,
  image_size=(img_height, img_width),
  batch_size=batch_size)

conv_layers= 1
conv_size=[32, 144, 96] 
dense_layers=1 
dense_size=[240, 240, 80]

for x in range(2):
    NAME = 'model-'+ str(conv_layers) + '-conv_layers-' + str(dense_layers) + 'dense_layers-' + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    log_dir = "logs/fit/" + NAME
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)

    model = build_model(conv_layers, conv_size, dense_layers, dense_size)
    model.fit(
    train_ds,
    validation_data=val_ds,
    epochs=4,
    callbacks=[tensorboard_callback]
    )
    tf.keras.models.save_model(model, '../model/final_model_'+ str(x))